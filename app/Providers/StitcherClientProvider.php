<?php declare(strict_types=1);

namespace App\Providers;

use Adduc\Stitcher\Client;
use DateTime;
use Exception;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Promise\FulfilledPromise;
use Illuminate\Support\ServiceProvider;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Kevinrob\GuzzleCache\CacheEntry;
use function GuzzleHttp\Psr7\build_query;

/**
 * @todo serve stale entry on failure to fetch
 */
class StitcherClientProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function ($app) {
            $stack = HandlerStack::create();
            $stack->setHandler(\GuzzleHttp\choose_handler());
            $stack->push($this->getHandler());

            $client = new Client([
                'stitcher' => [
                    'key' => env('STITCHER_API_KEY')
                ],
                'handler' => $stack,
                'connect_timeout' => 3,
                'headers' => [
                    'User-Agent' => 'Podcasts/0.1 Unofficial Stitcher RSS',
                ]
            ]);

            return $client;
        });
    }

    protected function getHandler(): \Closure
    {
        return function (callable $handler) {
            return function (RequestInterface $request, array $options) use ($handler) {

                $filestore = new \Illuminate\Cache\FileStore(
                    new \Illuminate\Filesystem\Filesystem(),
                    storage_path('cache/stitcher')
                );

                $cache = new \Illuminate\Cache\Repository($filestore);

                $query = \GuzzleHttp\Psr7\parse_query($request->getUri()->getQuery());

                $cache_request = $request;

                if ($query['uid'] ?? null) {
                    unset($query['uid']);
                    $uri = $request->getUri()->withQuery(build_query($query));
                    $cache_request = $request->withUri($uri);
                }

                $key = md5($request->getMethod() . ' ' . $cache_request->getUri());

                $value = $cache->get($key);

                if ($value && !$value->isStale()) {
                    return new FulfilledPromise($value->getOriginalResponse());
                }

                /**
                 * @param ResponseInterface $response
                 * @return ResponseInterface
                 */
                $success = function (ResponseInterface $response) use ($cache, $key, $options, $request) {
                    // default to 60 minutes
                    $ttl = isset($options['cache_ttl']) ? $options['cache_ttl'] : 60 * 60;

                    if ($ttl === 0) {
                        $datetime = new DateTime("+10 years");
                    } else {
                        $datetime = new DateTime("+{$ttl} seconds");
                    }

                    $entry = new CacheEntry($request, $response, $datetime);

                    // Save to cache forever because we need to be able to fall
                    // back to stale entry if fresh fetch fails
                    $result = $cache->forever($key, $entry);

                    // Cache, return response
                    return $response;
                };

                /**
                 * @param Exception $e
                 * @return ResponseInterface
                 */
                $failure = function (Exception $e) use ($cache, $key, $request, $value) {
                    app('log')->debug((string)$e);
                    if (!$value) {
                        throw $e;
                    }
                    app('log')->debug("Serving stale entry: {$request->getUri()}");
                    // If request fails, fallback to stale entry and
                    // cache for five minutes before next attempt.
                    $datetime = new DateTime("+5 minutes");
                    $response = $value->getOriginalResponse();
                    $entry = new CacheEntry($request, $response, $datetime);
                    $result = $cache->forever($key, $entry);
                    return $response;
                };

                return $handler($request, $options)->then($success, $failure);
            };
        };
    }
}
