<?php declare(strict_types=1);

namespace App\Tests;

use Laravel\Lumen\Testing\TestCase as TestCaseParent;

class TestCase extends TestCaseParent
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
