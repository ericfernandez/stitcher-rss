@extends('layout')

@section('content')
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter has-text-centered">
        <form method="GET">
            <div class="field has-addons">
                <div class="control is-expanded">
                    <input class="input" name="term" type="text" placeholder="Find a show">
                </div>
                <div class="control">
                    <input type="submit" value="Search" class="button is-primary">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
